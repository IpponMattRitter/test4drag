import { Test4dragPage } from './app.po';

describe('test4drag App', () => {
  let page: Test4dragPage;

  beforeEach(() => {
    page = new Test4dragPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
